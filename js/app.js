const DEBUG = true,
    ELE = function(sel) {
        return document.createElement(sel);
    },
    TXT = function(sel) {
        return document.createTextNode(sel);
    },
    $ = function(sel) {
        return document.querySelector(sel);
    },
    PID = function() {
        return new Date().getMilliseconds() + Math.random();
    }

class DOMElement {
    constructor() {}

    Listen(e, fn) {
        this.Element.addEventListener(e, fn);
    }

    Get(prop) {
        return this[prop];
    }
}

class Button extends DOMElement {
    constructor() {
        super();
    }
}

class StartButton extends Button {
    constructor(startMenu, icon) {
        super();
        this.Element = startMenu.querySelector('.label');
        this.AssignIcon(icon);
    }

    AssignIcon(icon) {
        this.Element.querySelector('.fa').classList.add(icon);
    }
}

class StartMenu extends DOMElement {
    constructor() {
        super();
        this.Element = document.querySelector('.start')
        this.StartButton = new StartButton(this.Element, 'fa-cogs');
        this.AssignStartButtonEvents();
    }

    AssignStartButtonEvents() {
        this.StartButton.Listen('click', (evt) => this.StartButtonClick(evt));
    }

    StartButtonClick(evt) {
        console.log(evt);
        this.Element.classList.toggle('active');
    }
}

class AppWindow {
    constructor(app) {
        let appIcon = ELE('i');
        let appName = TXT(app.Name);

        this.Element = document.createElement('div');
        this.Element.classList.add('application');

        appIcon.classList.add('fa', 'fa-file-text-o');

        this.Element.appendChild(appIcon)
        this.Element.appendChild(appName);

        $('.applications').appendChild(this.Element);
    }

}


class Application {
    constructor(name = 'Unnamed App', pid, icon = 'fa-file-text-o') {
        this.Name = name;
        this.Icon = icon;
        this.Pid = pid;

        this.ApplicationIcon = new ApplicationIcon(this);
        this.StartMenuItem = new StartMenuItem(this);
        // new ApplicationIcon();
    }

    Run() {
        this.AppWindow = new AppWindow(this);
    }
}

class ApplicationIcon {
    constructor(app = null) {
        this.GenerateIcon(app);
    }

    GenerateIcon(app) {
        this.Element = document.createElement('div');
        this.Element.classList.add('fa', app.Icon);
        document.querySelector('.desktop').appendChild(this.Element)
    }
}

class StartMenuItem {
    constructor(app) {
        this.GenerateStartMenuItem(app);
    }

    GenerateStartMenuItem(app) {
        // <div
        //  class="application"
        //  data-tooltip="APP_NAME"
        //  data-tooltip-position="right middle">
        //    <i class="fa fa-APP_ICON"></i>
        //    APP_NAME
        // </div>

        let appIcon = ELE('i');
        let appName = TXT(app.Name);

        console.log(app);

        this.Element = document.createElement('a');
        this.Element.classList.add('application');
        this.Element.setAttribute('href', 'javascript:bOs.Run(' + app.Pid + ')');

        appIcon.classList.add('fa', 'fa-file-text-o');

        this.Element.appendChild(appIcon)
        this.Element.appendChild(appName);

        $('.applications').appendChild(this.Element);

        this.Tooltip = new Tooltip({
            target: this.Element,
            position: 'right middle',
            content: app.Name
        });

    }
}


class Os {
    constructor() {
        this.StartMenu = new StartMenu();
        this.StartApps();
    }

    StartApps() {

        // This all should be in a loop.

        this.Apps = new Map();

        let App1 = {
            Pid: PID(),
            Name: 'Adobe Creative Suite Awesome Amazing 8'
        }
        let App2 = {
            Pid: PID(),
            Name: 'Axure RP'
        }
        this.Apps.set(App1.Pid, new Application(App1.Name, App1.Pid));
        this.Apps.set(App2.Pid, new Application(App2.Name, App2.Pid));
    };

    Run(pid) {
        this.Apps.get(pid).Run();
    }
}

/**
 * Fire up the application
 * @type {Os}
 */
document.addEventListener("DOMContentLoaded", function() {
    window.bOs = new Os();
});
